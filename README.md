# 8 plus 1 skills to becoming a web developer

There are many paths to becoming a software developer. This was mine.

_This is meant to be a low-theory, high-application course. It's not an easy path if you like knowledge to be spoon-fed lol_

I highly recommend that for every part of this learning path, that you find somebody (including yourself) to help with each new skill. example: Build your mom a website with steps 1 and 2. make a todo app with step 3. Each new skill you learn will enable you to solve new life and business problems!

## If you are super green, [Read the intro](./0-INTRO/1.md)

# Chapter 1: Hello World

_recommend creating a codepen.io for the chapter 1_
[link to blank codepen](https://codepen.io/pen/)

## 1 - basic html - [Link](./1-HTML/1.md)

- basic template
- link files
- link scripts
- semantic elements
- links

## 2 - core css - [Link](./2-CSS/1.md)

- ids
- classes
- selectors
- never use !important
- further studies

# Chapter 1.5: INPUT! (the really fun stuff) [Link](./3-JS/1.md)

![INPUT](https://cdn.shopify.com/s/files/1/0146/7714/7712/articles/Johnny5_150x150.jpg)

## 3 - modern js

- document.queryselector()
- User Input!
- variables
- loops
- functions

# Chapter 2: Bundles of Joy!

_learning how to develop the web locally on your computer_

- modern frameworks
- svelte hello world
- react hello world

## 4 - git

- create a repo
- make a commit
- push to repo
- pull from repo
- make a branch
- make a pr
- merge pr
- gitignore

## 5 - node

- npm init
- npm install
- npx
- package.json
- parcel
- bundle up a project and serve it

# Chapter 3: Liftoff

_servers and deploying your own web apps_

## 6 - servers

- express
- web server hello world
- basic routes
- restful routes
- CORS
- static assets

## 7 - database

- table
- columns
- data types
- add items
- delete items
- select
- foreign keys
- select join statement

# Chapter 4: the 10,000th step.

## 8 - MVC pattern

- what is a design pattern
- model
- view
- controller
- the dankest of patterns
- rails / django

# Chapter 5: Step One.

_the crap they don't teach you in school - ironically it's the most valuable thing a developer can know_

## plus one

- identifying problems
- finding shortest path to solving them
- it doesn't need to be original
- what is business value
- learn often.

# EXTRA

### \*Devops (not needed, but nice to know.)

- deploying your app
- containers
- other stuff

### Agile Cloud Serverless SASS Scrum AI

- buzzwords and crap
