# Challenge 1 - The HTML Element

### What is it?

An HTML element is basically a container that holds stuff. An element can hold text, it can make links, and show images. An element is generally made up of two "tags"; an opening tag (for example: `<div>`) and closing tag (`</div>`).

The two elements in this challenge are `<div>` and `<p>` elements. Conventionally, the 'p' element is meant to hold a paragraph of text, and a 'div' element is generally used to hold other HTML elements, such as 'p' elements!

In the next few lessons, you will be getting familiar with a bunch of new HTML tags. So Prepare your body. [Here is a list of all the HTML elements that browsers expect](https://www.tutorialrepublic.com/html-reference/html5-tags.php)

### The More You Know

- for every opening HTML element (for example: `<div>`) you must have a matching closing tag (`</div>`)!
- it is recommended you use the html tags that a are specified by the HTML overlords. so don't go creating a bunch of `<dank>` elements.

### The Challenge:

- Make a codepen that has a `<div>` element with 3 `<p>` elements inside of them. In between the `<p>` and `</p>` tags, put some text!

### Extra Wet Feet:

- Try putting divs inside of divs!

### Online HTML Editors

[Open A Blank jsFiddle](https://jsfiddle.net/) - if you forget to close a tag, or cross-nest elements, this one will tell you

[Open A Blank Codepen](https://codepen.io/pen/) - this one is most simple, but it doesn't tell you if you did anything wrong.

[Open A New CodeSandbox](https://codesandbox.io/s) - This one is more complicated, choose "Static template" put all the code inside the `<body>` element

# Navigation

[Main Menu](../README.md#1-basic-html-link) - [Next](./2.md)
